import { Router } from 'express';

import {
  getClients,
  postClient,
  deleteClient,
} from '../controllers/clients.controller';

const router = Router();

router.route('/').get(getClients).post(postClient);

router
  .route('/:id')
  .delete(deleteClient);

export default router;
