import { Router } from 'express';

import {
  getMeditions,
  postMedition,
  deleteMedition,
} from '../controllers/meditions.controller';

const router = Router();

router.route('/').get(getMeditions).post(postMedition);

router.route('/:id').delete(deleteMedition);

export default router;
