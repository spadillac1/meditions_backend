import express from 'express';
import cors from 'cors';

import clients from './routes/clients';
import meditions from './routes/meditions';

const app = express();

app.set('port', process.env.PORT || 5500);
app.use(cors());
app.use(express.json());

app.use('/api/clients', clients);
app.use('/api/meditions', meditions);

export { app };
