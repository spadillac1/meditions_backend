import { initializeFirebase } from './helpers/firebase';

export const initializeDatabaseSystem = () => {
  initializeFirebase();
  console.log('database was initialized');
};
