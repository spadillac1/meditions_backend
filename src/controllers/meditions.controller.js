import { firebaseGet, firebaseSave, firebaseDelete } from '../helpers/firebase';
import { collections } from '../helpers/enviroment';
import { convertData } from '../helpers/calcs';

export const getMeditions = (req, res) => {
  firebaseGet(collections.meditions)
    .then((data) =>{console.log(data); res.send(data)})
    .catch((error) => {
      res.status(300);
      res.send(error.message);
    });
};

export const postMedition = (req, res) => {
  firebaseSave(convertData(req.body), collections.meditions)
    .then(() => res.send('Document was added'))
    .catch((error) => {
      console.log(error.message);
      res.status(300);
      res.send(error.message);
    });
};

export const deleteMedition = (req, res) => {
  const { id } = req.params;
  firebaseDelete(id, collections.meditions)
    .then(() => res.send('Document was deleted'))
    .catch((error) => {
      res.status(400);
      res.send(error.message);
    });
};
