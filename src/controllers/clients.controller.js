import { collections } from '../helpers/enviroment';
import { firebaseGet, firebaseSave, firebaseDelete } from '../helpers/firebase';

export const getClients = (req, res) => {
  firebaseGet(collections.users)
    .then((data) => res.send(data))
    .catch((error) => {
      res.status(300);
      res.send(error.message);
    });
};

export const postClient = (req, res) => {
  firebaseSave(req.body, collections.users)
    .then(() => res.send('Document was added'))
    .catch((error) => {
      console.log(error.message);
      res.status(300);
      res.send(error.message);
    });
};

export const deleteClient = (req, res) => {
  const { id } = req.params;
  firebaseDelete(id, collections.users)
    .then(() => res.send('Document was deleted'))
    .catch((error) => {
      res.status(400);
      res.send(error.message);
    });
};
