const coordenatsToMts2 = (
  coordenada1,
  coordenada2,
  coordenada3,
  coordenada4
) => {
  let result = coordenada1 + coordenada2 + coordenada3 + coordenada4;
  return result.toFixed(3);
};

const coordenadaToPoint = (coordenada1, coordenada2) => {
  let point1 = 0;
  let point2 = 0;
  let result = 0;
  if (coordenada1.lat > coordenada2.lat) {
    point1 = coordenada1.lat - coordenada2.lat;
  } else {
    point1 = coordenada2.lat - coordenada1.lat;
  }
  if (coordenada1.lng > coordenada2.lng) {
    point2 = coordenada1.lng - coordenada2.lng;
  } else {
    point2 = coordenada2.lng - coordenada2.lng;
  }
  if (point1 > point2) {
    result = point1 - point2;
  } else {
    result = point2 - point1;
  }
  return result;
};

export const convertData = ({ clientName, nombreMedicion, coordenadas }) => {
  const { coordenada1, coordenada2, coordenada3, coordenada4 } = coordenadas;

  let _coordenada1 = coordenadaToPoint(coordenada1, coordenada2);
  let _coordenada2 = coordenadaToPoint(coordenada2, coordenada3);
  let _coordenada3 = coordenadaToPoint(coordenada3, coordenada4);
  let _coordenada4 = coordenadaToPoint(coordenada4, coordenada1);
  const mts2 = coordenatsToMts2(
    _coordenada1,
    _coordenada2,
    _coordenada3,
    _coordenada4
  );

  return {
    name: nombreMedicion,
    clientName,
    mts2,
    coordenada1: _coordenada1.toFixed(3),
    coordenada2: _coordenada1.toFixed(3),
    coordenada3: _coordenada1.toFixed(3),
    coordenada4: _coordenada1.toFixed(3),
  };
};
