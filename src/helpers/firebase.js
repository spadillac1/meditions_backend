import firebase from 'firebase';
import 'firebase/firestore';

import { collections } from './enviroment';

let db = null;
const firebaseConfig = {
  apiKey: 'AIzaSyDydFcYmyJvhLy3xTg57CeRydtJPJlMupM',
  authDomain: 'meditions-a820d.firebaseapp.com',
  databaseURL: 'https://meditions-a820d.firebaseio.com',
  projectId: 'meditions-a820d',
  storageBucket: 'meditions-a820d.appspot.com',
  messagingSenderId: '966558518292',
  appId: '1:966558518292:web:76926c83fa33b055632b01',
};

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
  db = firebase.firestore();
};

export const firebaseSave = async (data, collection) => {
  return db.collection(collection).add(data);
};

export const firebaseGet = (collection) => {
  return db
    .collection(collection)
    .get()
    .then((querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => data.push(doc.data()));
      return data;
    });
};

export const firebaseDelete = (id, collection) => {
  return db.collection(collection).doc('DC').delete(id);
};

export const getUserIdByName = (userName) => {
  return db.collection(collections.users).where(name, '==', userName).get();
  /*     .then(function (querySnapshot) { */
  // querySnapshot.forEach(function (doc) {
  // return doc.data();
  // });
  // })
  // .catch(function (error) {
  // console.log('Error getting documents: ', error);
  /* }); */
};
